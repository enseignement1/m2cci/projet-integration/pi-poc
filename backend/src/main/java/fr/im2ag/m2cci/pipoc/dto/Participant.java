package fr.im2ag.m2cci.pipoc.dto;

public record Participant(int id, String nom, String prenom, int nbreStops) {
}
